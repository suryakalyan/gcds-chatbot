# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"
from functions import *
from pymongo import MongoClient
from rasa_sdk import Action, Tracker
from typing import Any, Text, Dict, List
from rasa_sdk.executor import CollectingDispatcher

client = MongoClient("localhost", 27017)
db = client["CPT-DEV"]
db.authenticate("cpt-dev", "cpt2019!gotomoon")


class KeyWordValidaton(Action):
    def name(self) -> Text:
        return "key_word_validation"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List:
        print(tracker.latest_message['entities'])
        keyword, study_number, date = question_validation(tracker.latest_message['entities'])
        #print("keyword:  ", keyword)
        if len(keyword) > 0:
            keyWord = check_key_word(keyword)
            if len(keyWord) == 0:
                dispatcher.utter_message(text="not a valid keyword {}".format(keyWord))
        else:
            dispatcher.utter_message(text="provide keyword")


class StudyNumberValidaton(Action):
    def name(self) -> Text:
        return "study_number_and_date_validation"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List:
        # print(tracker.latest_message['entities'])
        keyword, study_number, date = question_validation(tracker.latest_message['entities'])
        collection = db.get_collection('Study_Summary')
        if study_number is not None:
            if date is not None:
                if (collection.find_one({"CPT STUDY NUMBER": study_number})) is not None and len(
                        collection.find_one({"CPT STUDY NUMBER": study_number})) > 0:
                    date = date_validations(date)
                    result = list(collection.find({"CPT STUDY NUMBER": study_number, "CPT REPORT DATE": date[0]}))
                    if len(result) > 0:
                        g_study_summary = fetch_data("Study_Summary", study_number,
                                                     date[0], db)

                        keyWord = check_key_word(keyword)
                        print("keyword:  ", (tracker.latest_message)['text'])
                        response = find_result_study(keyword=keyWord[0], ID=study_number, date=date,
                                                     df=g_study_summary)
                        dispatcher.utter_message(
                            text="bot : {} for study number {} -[{}] is: {}".format(keyword.lower(), study_number,
                                                                                    date[0], response))
                    else:
                        avaliable_dates = avaliable_date(study_number)
                        dispatcher.utter_message(text="list of avaliable dates : {}".format(avaliable_dates))
                else:
                    all_study_numbers = collection.distinct("CPT STUDY NUMBER")
                    study_list = multi_line_print(all_study_numbers)
                    dispatcher.utter_message(text="avaliable study list are: {}".format(study_list))
            else:
                avaliable_dates = avaliable_date(study_number)
                dispatcher.utter_message(text="list of avaliable dates : {}".format(avaliable_dates))
        else:
            all_study_numbers = collection.distinct("CPT STUDY NUMBER")
            study_list = multi_line_print(all_study_numbers)
            dispatcher.utter_message(text="avaliable study list are: {}".format(study_list))

# class DateValidaton(Action):
#     def name(self) -> Text:
#         return "date_validation"
#
#     def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List:
#         keyword, study_number, date = question_validation(tracker.latest_message['entities'])
#         if date is not None:
#             date = []
#             try:
#                 t = dparser.parse(date, fuzzy=True)
#                 t = t.strftime("%m/%d/%Y")
#                 if len(date) >= 1:
#                     date.pop(0)
#                 date.insert(0, t)
#                 print(date)
#             except:
#                 pass
#             collection = db.get_collection('Study_Summary')
#             result = list(collection.find({"CPT STUDY NUMBER": study_number, "CPT REPORT DATE": date}))
#             # if len(result) > 0:
#             #     dispatcher.utter_message(text="valid date")
#             # else:
#             if len(result) < 0:
#                 avaliable_dates = avaliable_date(study_number)
#                 dispatcher.utter_message(text="list of avaliable dates : {}".format(avaliable_dates))
#         else:
#             avaliable_dates = avaliable_date(study_number)
#             dispatcher.utter_message(text="list of avaliable dates : {}".format(avaliable_dates))
#

# class BotResponse(Action):
#     def name(self) -> Text:
#         return "bot_response"
#
#     def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List:
#         keyword, study_number, date = question_validation(tracker.latest_message['entities'])
#         if keyword and study_number and date:
#             g_study_summary = fetch_data("Study_Summary", study_number,
#                                          date, db)
#             keyWord = check_key_word(keyword)
#             print("keyword:  ", (tracker.latest_message)['text'])
#             response = find_result_study(keyword=keyWord[0], ID=study_number, date=date,
#                                          df=g_study_summary)
#             print("response:    ", response)
#             dispatcher.utter_message(
#                 text="bot : {} for study number {} -[{}] is: {}".format(keyword.lower(), study_number,
#                                                                   date, response))
#         else:
#             dispatcher.utter_message(text="bot : please provide a valid data")
