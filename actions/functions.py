import re
import pandas as pd
from pymongo import MongoClient
from collections import ChainMap
import dateutil.parser as dparser
from other.data.keyword_dict import  keyword_unit

client = MongoClient("localhost", 27017)
db = client["CPT-DEV"]
db.authenticate("cpt-dev", "cpt2019!gotomoon")


def date_validations(input_date):
    print(input_date)
    date = []
    try:
        t = dparser.parse(input_date, fuzzy=True)
        t = t.strftime("%m-%d-%Y")
        if len(date) >= 1:
            date.pop(0)
        date.insert(0, t)
    except:
        pass
    print("result_date:  ", date)
    return date


def question_validation(entities):
    key_word = None
    study_number = None
    date = None
    text = ""
    #flag = 0
    for entity in entities:
        if entity['entity'] == 'key_word':
            #if flag == 0:
            text = text + entity['value']
            # else:
            #     text = text + ' > ' + entity['value']
            # flag = flag + 1
        if entity['entity'] == 'study_number':
            study_number = entity['value']

        if entity['entity'] == 'date':
            date = entity['value']
    key_word = text
    return key_word, study_number, date


def avaliable_date(study_number):
    collection = db.get_collection('Study_Summary')
    all_date = collection.aggregate([{"$match": {"CPT STUDY NUMBER": study_number}},
                                     {"$group": {"_id": None, "date": {"$addToSet": "$CPT REPORT DATE"}}},
                                     {"$project": {"_id": 0, "date": 1}}]).next()['date']
    avaliable_dates = multi_line_print(all_date)
    return avaliable_dates


# parsing date from the columns
def date_parser(df, columns_list):
    for column in columns_list:
        df[column] = pd.to_datetime(df[column]).dt.strftime('%m/%d/%Y')
    return df


def fetch_data(collection, study_number, date, db):
    record = list(db[collection].find({"CPT STUDY NUMBER": study_number, "CPT REPORT DATE": date}))
    if len(record) == 1:
        df = pd.DataFrame.from_dict(record[0], orient='index').T
    else:
        df = pd.DataFrame.from_dict(record, orient='columns')
    # checking the row count
    if df.shape[0] >= 1:
        coumns_list = ["LAST VISIT DATE", "LAST CONTACT DATE", "STUDY TERMINATION DATE",
                       "DATE OF LAST MEDICAL QUERY ISSUED"]
        df = date_parser(df, coumns_list)
    print(collection, "\n", df.head())
    df.rename(columns={"% MISSING PAGES": "PERCENTAGE OF MISSING PAGES",
                       "% MISSING PAGES FOR MORE THAN 10 DAYS": "PERCENTAGE OF MISSING PAGES FOR MORE THAN 10 DAYS",
                       "% OPEN QUERIES (AT SITE)": "PERCENTAGE OF OPEN QUERIES (AT SITE)",
                       "% OPEN QUERIES FOR MORE THAN 14 DAYS (AT SITE)": "PERCENTAGE OF OPEN QUERIES FOR MORE THAN 14 DAYS (AT SITE)",
                       "% ANSWERED QUERIES FOR MORE THAN 14 DAYS": "TOTAL PERCENTAGE OF ANSWERED QUERIES FOR MORE THAN 14 DAYS",
                       "% ANSWERED QUERIES": "PERCENTAGE OF ANSWERED QUERIES"}, inplace=True)
    return df


def match_keyunit(keyword):
    """return unit for keyword"""
    # TODO: can use get function instead
    print("match key unt")
    if keyword in keyword_unit.keys():
        return keyword_unit[keyword]
    else:
        return ""


def multi_line_print(return_list):
    print("multi line")
    text = ""
    for line in return_list:
        text = text + line + "<br>"
    return text


def find_result_study(keyword, ID, date, df):
    """Filter dataframe by study number
    Param:
        keyword: column name(query key)
        ID - study number
        date - study date
        df - dataframe at study level
    """
    print("Zeroooo")
    df = df[df["CPT STUDY NUMBER"] == ID]
    if df.empty:
        return None
    else:
        try:
            result = str(df[keyword].tolist()[0]) + match_keyunit(keyword)
            print("result:   ", result)
            if result == "nan" or result == 'None':
                result = "not given"
            return result
        except Exception as e:
            return str(e)


def check_key_word(sentence):
    keys = []
    keyWord = []
    keyMulti = []
    lst = db.keyword_synonyms.find({})
    lst = list(lst)
    keyword_dict = dict(ChainMap(*[lst[0]['keyword_dict']]))
    classes = dict(ChainMap(*[lst[0]['classes']]))
    sentence = sentence.lower()
    for key, value in keyword_dict.items():
        for word in value:
            word = word.lower()
            if len(re.findall(word, sentence)) > 1:
                keyMulti.append(key)
            elif len(re.findall(word, sentence)) == 1:
                keys.append(key)
    # replace set with np.unique
    keys = list(set(keys))
    keyMulti = list(set(keyMulti))
    for key, value in classes.items():
        temp = []
        temp1 = []
        for word in value:
            if word in keys:
                temp.append(word)
            if word in keyMulti:
                temp1.append(word)
        if len(temp) > 0:
            keyWord.append(max(temp, key=len))
        if len(temp1) > 0:
            keyWord.append(max(temp1, key=len))
    return keyWord
